package com.phossil.week11;

public class Superman extends Human implements Flyable{

    public Superman(String name) {
        super(name);
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat.");
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep.");
    }
    
    @Override
    public String toString() {
        return "Superman ("+this.getName()+")";
    }

    @Override
    public void takeoff() {
        System.out.println(this.toString() + " takeoff.");
        
    }

    @Override
    public void fly() {
        System.out.println(this.toString() + " fly.");
        
    }

    @Override
    public void landing() {
        System.out.println(this.toString() + " landing.");
        
    }
}
