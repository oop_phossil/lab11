package com.phossil.week11;

public class Submarine extends Vehicle implements Swimable {

    public Submarine(String name) {
        super(name, "mtu Series 12V 4000");
    }

    @Override
    public void swim() {
        System.out.println(this.toString() + " swim.");
        
    }

    @Override
    public String toString() {
        return "Submarine (" + this.getName() + ")";
    }
    
}
