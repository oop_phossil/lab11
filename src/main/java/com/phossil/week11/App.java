package com.phossil.week11;


public class App 
{
    public static void main( String[] args )
    {
        Bird bird1 = new Bird("Twitty");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        Plane boeing = new Plane("boeing", "Rosaroi");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();
        Superman clark = new Superman("clark");
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.walk();
        clark.run();
        clark.eat();
        clark.sleep();
        clark.swim();
        Human man1 = new Human("Man");
        man1.eat();
        man1.sleep();
        man1.walk();
        man1.run();
        man1.swim();
        Bat bat = new Bat("bat");
        bat.eat();
        bat.sleep();
        bat.takeoff();
        bat.fly();
        bat.landing();
        Bus ns132 = new Bus("ns132");
        Cat carfield = new Cat("Carfield");
        carfield.eat();
        carfield.sleep();
        carfield.walk();
        carfield.run();
        Dog bull = new Dog("Bull");
        bull.eat();
        bull.sleep();
        bull.walk();
        bull.run();
        Fish nemo = new Fish("nemo");
        nemo.swim();
        nemo.eat();
        nemo.sleep();
        Rat jerry = new Rat("jerry");
        jerry.eat();
        jerry.sleep();
        jerry.walk();
        jerry.run();
        Snake baishu = new Snake("baishu");
        baishu.crawl();
        baishu.eat();
        baishu.sleep();
        Crocodile aligator = new Crocodile("aligator");
        aligator.crawl();
        aligator.eat();
        aligator.sleep();
        aligator.swim();
        Submarine yuth = new Submarine("yuth");
        yuth.swim();

        
        Flyable[] flyables = {bird1, boeing, clark};
        for (int i = 0; i < flyables.length; i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }
        Walkable[] walkables = {bird1, man1, clark};
        for (int i = 0; i < walkables.length; i++) {
            walkables[i].walk();
            walkables[i].run();
        }

        Swimable[] swimables = {clark, man1, nemo, aligator,yuth};
        for (int i = 0; i < swimables.length; i++) {
            swimables[i].swim();
        }

        Crawlable[] crawlables = {baishu, aligator};
        for (int i = 0; i < crawlables.length; i++) {
            crawlables[i].crawl();
        }
    }
}
