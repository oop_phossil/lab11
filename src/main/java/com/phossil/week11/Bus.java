package com.phossil.week11;

public class Bus extends Vehicle{

    public Bus(String name) {
        super(name, "Mercedes O303");
    }
    
    @Override
    public String toString() {
        return "Bus (" + this.getName() + ")";
    }
}
